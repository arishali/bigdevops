"use strict";

const MongoClient = require("mongodb").MongoClient;
const { Validator } = require("node-input-validator");
const connection_uri = "mongodb://localhost/bigImmersive";

exports.handler = async (event, context) => {
  console.log("Received event:", JSON.stringify(event, null, 2));
  const client = new MongoClient(connection_uri, { useNewUrlParser: true });
  await client.connect();
  let collection = await client.db("bigImmersive").collection("emails");

  let body;
  let statusCode = "200";
  const headers = {
    "Content-Type": "application/json",
  };

  try {
    switch (event.httpMethod) {
      case "POST":
        body = await processEvent(event, collection);
        statusCode = body.status;
        break;

      default:
        throw new Error(`Unsupported method "${event.httpMethod}"`);
    }
  } catch (err) {
    statusCode = "400";
    body = err.message;
    client.close();
  } finally {
    body = JSON.stringify(body);
  }
  client.close();
  return {
    statusCode,
    body,
    headers,
  };
};

async function processEvent(event, collection) {
  let validate = await validateInput(event);
  let body;
  if (validate.status == 200) {
    let doc = await collection.findOne({
      emailId: event.body.emailAddress,
    });
    if (doc && doc.timeStamp + 60000 > new Date().getTime()) {
      //throw error
      body = {
        msg: "cannot send this email again for 1 min",
        status: "400",
      };
    } else {
      if (doc) {
        await collection.updateOne(
          { emailId: event.body.emailAddress },
          { $set: { timeStamp: new Date().getTime() } }
        );
      } else {
        await collection.insertOne({
          emailId: event.body.emailAddress,
          timeStamp: new Date().getTime(),
        });
      }

      body = {
        msg: "email sent",
        status: "200",
      };
    }
  } else {
    body = {
      msg: validate.error,
      status: validate.status,
    };
  }
  return body;
}
async function validateInput(event) {
  const v = new Validator(event.body, {
    emailAddress: "required|email",
    type: "required|string",
  });

  const matched = await v.check();
  let response;
  if (!matched) {
    response = {
      status: 401,
      error: v.errors,
    };
  } else if (
    event.body.type == "forget.password" ||
    event.body.type == "verify_email"
  ) {
    response = {
      status: 200,
    };
  } else {
    response = {
      status: 401,
      error: "Unsupported type",
    };
  }
  return response;
}
